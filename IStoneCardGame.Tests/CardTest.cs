﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Game;

namespace IStoneCardGame.Tests
{
    [TestClass]
    public class CardTest
    {
        [TestMethod]
        public void ComplexCardTest()
        {
            Card Card1 = new Card(CardColor.Clubs, CardValue.Five);
            Card Card2 = new Card(CardColor.Hearts, CardValue.Queen);
            Card Card3 = new Card(CardColor.Spades, CardValue.Ace);

            String ExpectedCard1Name  = "Clubs Five";
            String ExpectedCard2Name  = "Hearts Queen";
            String ExpectedCard3Name = "Spades Ace";
            int ExpectedCard1IntValue = 5;
            int ExpectedCard2IntValue = 12;
            int ExpectedCard3IntValue = 14;

            Assert.AreEqual(ExpectedCard1Name, Card1.Identify());
            Assert.IsTrue(ExpectedCard1IntValue == Card1.GetIntValue());

            Assert.AreEqual(ExpectedCard2Name, Card2.Identify());
            Assert.IsTrue(ExpectedCard2IntValue == Card2.GetIntValue());

            Assert.AreEqual(ExpectedCard3Name, Card3.Identify());
            Assert.IsTrue(ExpectedCard3IntValue == Card3.GetIntValue());

        }
    }
}
