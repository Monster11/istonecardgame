﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Game;
using System.Linq;
using System.Collections.Generic;

namespace IStoneCardGame.Tests
{
    [TestClass]
    public class DeckTest
    {
        private int DeckShufflePercentQuality(Deck deckToTest)
        {
            int BadQualityCounter = 0;
            CardColor LastCardColor = CardColor.Hearts;
            CardValue LastCardValue = CardValue.King;

            foreach(Card card in deckToTest.GetCards())
            {
                if (card.GetValue() == CardValue.Ace && LastCardValue == CardValue.King)
                    LastCardColor = card.GetColor();

                if (card.GetColor() == LastCardColor)
                {
                    if ((card.GetValue() == LastCardValue + 1) || (card.GetValue() == CardValue.Ace && LastCardValue == CardValue.King))
                        BadQualityCounter++;
                }

                LastCardValue = card.GetValue();
            }

            return (int)(((double)BadQualityCounter/(double)52)*100);
        }

        [TestMethod]
        public void ShuffleDeck()
        {
            Deck TestDeck = new Deck();

            TestDeck.Shuffle();
            int ShuffleQuality = DeckShufflePercentQuality(TestDeck);

            Assert.IsTrue(ShuffleQuality < 50);

        }

        [TestMethod]
        public void SortDeck()
        {
            Deck TestDeck = new Deck();

            TestDeck.Shuffle();
            TestDeck.Sort();

            int ShuffleQuality = DeckShufflePercentQuality(TestDeck);

            Assert.AreEqual(100, ShuffleQuality);
        }

        [TestMethod]
        public void DrawCard()
        {
            Deck TestDeck = new Deck();

            Card FirstCardInDeck = TestDeck.GetCards().First();
            Card DrawnCard = TestDeck.Draw;

            Assert.AreEqual(FirstCardInDeck, DrawnCard);
            Assert.IsTrue(TestDeck.CardsCount == 51);
        }

        [TestMethod]
        public void CountCards()
        {
            Deck TestDeck = new Deck();

            Assert.IsTrue(TestDeck.CardsCount == 52);
        }

        [TestMethod]
        public void LoadCards()
        {
            Deck TestDeck = new Deck(new List<Card>());
            List<Card> TestCards = new List<Card>();
            Card Card1 = new Card(CardColor.Clubs, CardValue.Five);
            Card Card2 = new Card(CardColor.Hearts, CardValue.Ten);
            Card Card3 = new Card(CardColor.Spades, CardValue.Ace);

            TestCards.Add(Card1);
            TestCards.Add(Card2);
            TestCards.Add(Card3);

            TestDeck.LoadCards(TestCards);

            bool Test1 = TestDeck.CardsCount == 3;

            Assert.IsTrue(Test1);
        }

        [TestMethod]
        public void AddCard()
        {
            Deck TestDeck = new Deck(new List<Card>());
            Card Card1 = new Card(CardColor.Clubs, CardValue.Five);

            TestDeck.AddCard(Card1);

            bool Test1 = TestDeck.CardsCount == 1;
            bool Test2 = TestDeck.GetCards().Contains(Card1);

            Assert.IsTrue(Test1);
            Assert.IsTrue(Test2);
        }

        [TestMethod]
        public void RemoveCard()
        {
            Deck TestDeck = new Deck(new List<Card>());
            List<Card> TestCards = new List<Card>();
            Card Card1 = new Card(CardColor.Clubs, CardValue.Five);
            Card Card2 = new Card(CardColor.Hearts, CardValue.Ten);
            Card Card3 = new Card(CardColor.Spades, CardValue.Ace);

            TestCards.Add(Card1);
            TestCards.Add(Card2);
            TestCards.Add(Card3);

            TestDeck.LoadCards(TestCards);
            TestDeck.RemoveCard(Card2);

            bool Test1 = TestDeck.GetCards().Contains(Card2);
            bool Test2 = TestDeck.CardsCount == 2;

            Assert.IsFalse(Test1);
            Assert.IsTrue(Test2);
        }

        [TestMethod]
        public void ClearDeck()
        {
            List<Card> TestCards = new List<Card>
            {
                new Card(CardColor.Clubs, CardValue.Five),
                new Card(CardColor.Hearts, CardValue.Ten),
                new Card(CardColor.Spades, CardValue.Ace)
            };

            Deck TestDeck = new Deck(TestCards);
            TestDeck.ClearDeck();

            bool Test1 = TestDeck.CardsCount == 0;

            Assert.IsTrue(Test1);
        }

        [TestMethod]
        public void DeckSplitTest()
        {
            Card Card1 = new Card(CardColor.Clubs, CardValue.Five);
            Card Card2 = new Card(CardColor.Hearts, CardValue.Ten);
            Card Card3 = new Card(CardColor.Spades, CardValue.Ace);

            List<Card> TestCards = new List<Card>();

            TestCards.Add(Card1);
            TestCards.Add(Card2);
            TestCards.Add(Card3);

            Deck TestDeck = new Deck(TestCards);
            Deck Deck1 = new Deck(new List<Card>());
            Deck Deck2 = new Deck(new List<Card>());

            DeckManager.SplitDeck(TestDeck, ref Deck1, ref Deck2);

            Assert.IsTrue(Deck1.GetCards().Contains(Card1));
            Assert.IsTrue(Deck1.GetCards().Contains(Card3));
            Assert.IsTrue(Deck2.GetCards().Contains(Card2));
            Assert.IsTrue(!Deck2.GetCards().Contains(Card3));
            Assert.IsTrue(Deck1.CardsCount == 2);
            Assert.IsTrue(Deck2.CardsCount == 1);
        }

        [TestMethod]
        public void FightTwoCards()
        {
            Card Card1 = new Card(CardColor.Hearts, CardValue.Ace);
            Card Card2 = new Card(CardColor.Hearts, CardValue.Queen);
            Card Card3 = new Card(CardColor.Spades, CardValue.Ace);

            bool Test1 = DeckManager.FightTwoCards(Card1, Card2) == LastGameResult.HostileWin;
            bool Test2 = DeckManager.FightTwoCards(Card1, Card3) == LastGameResult.Draw;

            Assert.IsTrue(Test1);
            Assert.IsTrue(Test2);
        }

        [TestMethod]
        public void ConnectDecks()
        {
            Card Card1 = new Card(CardColor.Hearts, CardValue.Ace);
            Card Card2 = new Card(CardColor.Hearts, CardValue.Queen);
            Card Card3 = new Card(CardColor.Spades, CardValue.Ace);

            Deck TestDeck = new Deck(new List<Card>());
            List<Card> TestStack1 = new List<Card>();
            List<Card> TestStack2 = new List<Card>();

            TestDeck.AddCard(Card1);
            TestStack1.Add(Card2);
            TestStack2.Add(Card3);

            TestDeck = DeckManager.ConnectDecks(TestDeck, TestStack1, TestStack2);

            bool Test1 = TestDeck.CardsCount == 3;
            bool Test2 = TestDeck.GetCards().Contains(Card1);
            bool Test3 = TestDeck.GetCards().Contains(Card2);
            bool Test4 = TestDeck.GetCards().Contains(Card3);

            Assert.IsTrue(Test1);
            Assert.IsTrue(Test2);
            Assert.IsTrue(Test3);
            Assert.IsTrue(Test4);
        }
    }
}
