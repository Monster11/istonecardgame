﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IStoneCardGame.Controllers;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IStoneCardGame;
using Moq;
using System.Web;

namespace IStoneCardGame.Tests.Controllers
{
    [TestClass]
    public class GameControllerTest
    {
        [TestMethod]
        public void Play()
        {
            var context = new Mock<ControllerContext>();
            var session = new Mock<HttpSessionStateBase>();

            context.Setup(m => m.HttpContext.Session).Returns(session.Object);

            GameController controller = new GameController();
            controller.ControllerContext = context.Object;

            // Act
            ViewResult result = controller.Play() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void LoadNewDeck()
        {
            var context = new Mock<ControllerContext>();
            var session = new Mock<HttpSessionStateBase>();

            context.Setup(m => m.HttpContext.Session).Returns(session.Object);

            GameController controller = new GameController();
            controller.ControllerContext = context.Object;

            // Act
            ViewResult result = controller.LoadNewDeck() as ViewResult;

            // Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void ShuffleDeck()
        {
            var context = new Mock<ControllerContext>();
            var session = new Mock<HttpSessionStateBase>();

            context.Setup(m => m.HttpContext.Session).Returns(session.Object);

            GameController controller = new GameController();
            controller.ControllerContext = context.Object;

            // Act
            ViewResult result = controller.ShuffleDeck() as ViewResult;

            // Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void SortDeck()
        {
            var context = new Mock<ControllerContext>();
            var session = new Mock<HttpSessionStateBase>();

            context.Setup(m => m.HttpContext.Session).Returns(session.Object);

            GameController controller = new GameController();
            controller.ControllerContext = context.Object;

            // Act
            ViewResult result = controller.SortDeck() as ViewResult;

            // Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void DivideDeck()
        {
            var context = new Mock<ControllerContext>();
            var session = new Mock<HttpSessionStateBase>();

            context.Setup(m => m.HttpContext.Session).Returns(session.Object);

            GameController controller = new GameController();
            controller.ControllerContext = context.Object;

            // Act
            ViewResult result = controller.DivideDeck() as ViewResult;

            // Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void DrawCards()
        {
            var context = new Mock<ControllerContext>();
            var session = new Mock<HttpSessionStateBase>();

            context.Setup(m => m.HttpContext.Session).Returns(session.Object);

            GameController controller = new GameController();
            controller.ControllerContext = context.Object;

            // Act
            ViewResult result = controller.DrawCards() as ViewResult;

            // Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }


    }
}
