﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Game
{
    public enum CardColor
    {
        Hearts = 1,
        Diamonds = 2,
        Clubs = 3,
        Spades = 4
    }

    public enum CardValue
    {
        Ace = 1,
        Two = 2,
        Three = 3,
        Four = 4,
        Five = 5,
        Six = 6,
        Seven = 7,
        Eight = 8,
        Nine = 9,
        Ten = 10,
        Jack = 11,
        Queen = 12,
        King = 13
    }

    public enum LastGameResult
    {
        Null = 0,
        PlayerWin = 1,
        HostileWin = 2,
        Draw = 3
    }

    public enum GameStatus
    {
        InGame = 1,
        HostileWon = 2,
        PlayerWon = 3

    }
}
