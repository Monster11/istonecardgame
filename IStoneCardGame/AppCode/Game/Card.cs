﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Game
{
    public class Card
    {
        private CardColor _color;
        private CardValue _value;

        public Card(CardColor color, CardValue value)
        {
            _color = color;
            _value = value;
        }

        public String Identify()
        {
            return String.Format("{0} {1}", _color, _value);
        }

        public CardColor GetColor()
        {
            return _color;
        }

        public String CardHtmlStringValue()
        {
            String FinalChar = String.Empty;
            String FinalColor = "black";
            switch (_value)
            {
                case CardValue.Ace:
                    FinalChar = "A";
                    break;
                case CardValue.King:
                    FinalChar = "K";
                    break;
                case CardValue.Queen:
                    FinalChar = "Q";
                    break;
                case CardValue.Jack:
                    FinalChar = "J";
                    break;
                default:
                    FinalChar = ((int)_value).ToString();
                    break;
            }

            if (_color == CardColor.Diamonds || _color == CardColor.Hearts)
                FinalColor = "red";
            

            return String.Format("<span style=\"color: {1};\">{0}</span>", FinalChar, FinalColor);
        }

        public CardValue GetValue() => _value;

        public int GetIntValue()
        {
            if (_value == CardValue.Ace)
                return 14;
            return (int)_value;
        }

    }
}