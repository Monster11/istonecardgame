﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Game
{
    public sealed class Deck : IDeck
    {
        private List<Card> _cards;

        public Deck()
        {
            _cards = DeckManager.ReturnNewDeck();
        }

        public Deck(List<Card> specifiedDeck)
        {
            _cards = specifiedDeck;
        }

        public void AddCard(Card card)
        {
            _cards.Add(card);
        }

        public void ClearDeck()
        {
            _cards.Clear();
        }

        public int CardsCount
        {
            get
            {
                return _cards.Count;
            }
        }

        public Card Draw
        {
            get
            {
                if (_cards.Count() == 0)
                    throw new Exception("Not expected, draw from empty deck");

                Card TopCard = _cards.First();
                RemoveCard(TopCard);

                return TopCard;
            }
        }

        public List<Card> GetCards()
        {
            return _cards;
        }

        public void LoadCards(List<Card> cards)
        {
            foreach(Card newCard in cards)
            {
                AddCard(newCard);
            }
        }

        public void RemoveCard(Card card)
        {
            _cards.Remove(card);
        }

        public void Shuffle()
        {
            _cards = DeckManager.ShuffledDeck(this);
        }

        public void Sort()
        {
            _cards = DeckManager.SortDeck(this);
        }
    }

    interface IDeck
    {
        void Shuffle();

        void Sort();

        Card Draw { get; }

        int CardsCount { get; }

        List<Card> GetCards();

        void RemoveCard(Card card);

        void AddCard(Card card);

        void LoadCards(List<Card> cards);

        void ClearDeck();
    }
}