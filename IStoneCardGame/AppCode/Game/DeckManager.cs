﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Game
{
    public static class DeckManager
    {
        public static List<Card> ReturnNewDeck()
        {
            List<Card> NewDeck = new List<Card>();

            foreach (CardColor cardColor in Enum.GetValues(typeof(CardColor)))
            {
                foreach (CardValue cardValue in Enum.GetValues(typeof(CardValue)))
                {
                    NewDeck.Add(new Card(cardColor, cardValue));
                }
            }

            return NewDeck;
        }

        public static List<Card> ShuffledDeck(Deck deck)
        {
            List<Card> CardsList = deck.GetCards();
            List<Card> RandomizedCards = new List<Card>();

            Random Randomizer = new Random();
            int RandomIndex = 0;
            while (CardsList.Count > 0)
            {
                RandomIndex = Randomizer.Next(0, CardsList.Count); 
                RandomizedCards.Add(CardsList[RandomIndex]); 
                CardsList.RemoveAt(RandomIndex); 
            }

            return RandomizedCards;
        }

        public static List<Card> SortDeck(Deck deck)
        {
            List<Card> CardsList = deck.GetCards();

            return CardsList
                .OrderBy(x => (int)x.GetColor())
                .ThenBy(x => (int)x.GetValue())
                .ToList();
        }

        public static void SplitDeck(Deck mainDeck, ref Deck deck1, ref Deck deck2)
        {
            List<Card> CardsList = mainDeck.GetCards();

            int counter = 0;
            foreach(Card card in CardsList)
            {
                if(counter%2==0)
                    deck1.AddCard(card);
                else
                    deck2.AddCard(card);

                ++counter;
            } 
        }

        public static LastGameResult FightTwoCards(Card hostileCard, Card playerCard)
        {
            if (hostileCard.GetIntValue() > playerCard.GetIntValue())
                return LastGameResult.HostileWin;
            else if (hostileCard.GetIntValue() < playerCard.GetIntValue())
                return LastGameResult.PlayerWin;
            else
                return LastGameResult.Draw;
        }

        public static Deck ConnectDecks(Deck MainDeck, List<Card> Cards1, List<Card> Cards2)
        {
            foreach (Card card in Cards1)
            {
                MainDeck.AddCard(card);
            }
            foreach (Card card in Cards2)
            {
                MainDeck.AddCard(card);
            }

            return MainDeck;
        }
    }
}