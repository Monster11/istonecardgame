﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Game;

namespace IStoneCardGame.Controllers
{
    public class GameController : Controller
    {
        private const String CardUrlTemplate = "<img src=\"/Images/Game/{0}.png\" />";

        [HttpGet]
        public ActionResult Play()
        {
            if (Session["GameStatus"] == null)
                Session["GameStatus"] = GameStatus.InGame;

            //Current battle cards
            List<Card> HostileStack = (List<Card>)Session["CurrentHostileStack"];
            List<Card> PlayerStack = (List<Card>)Session["CurrentPlayerStack"];

            if (HostileStack == null)
            {
                ViewBag.HostileCardColor = String.Format(CardUrlTemplate, "1");
                ViewBag.PlayerCardColor = String.Format(CardUrlTemplate, "2");
                ViewBag.HostileCardValue = ViewBag.PlayerCardValue = "?";
            }
            else
            {
                Card HostileCard = HostileStack.Last();
                ViewBag.HostileCardColor = String.Format(CardUrlTemplate, ((int)HostileCard.GetColor()));
                ViewBag.HostileCardValue = HostileCard.CardHtmlStringValue();

                Card PlayerCard = PlayerStack.Last();
                ViewBag.PlayerCardColor = String.Format(CardUrlTemplate, ((int)PlayerCard.GetColor()));
                ViewBag.PlayerCardValue = PlayerCard.CardHtmlStringValue();
            }


            //Check full deck
            if (Session["FullDeck"] == null)
                ViewBag.FullDeck = "empty.";
            else
                ViewBag.FullDeck = String.Format("currently {0} cards in main deck.", ((Deck)Session["FullDeck"]).CardsCount);

            #region CHEATS
            if (Session["FullDeck"] != null)
            {
                Deck MainDeck = (Deck)Session["FullDeck"];
                StringBuilder FullDeckInfo = new StringBuilder();
                FullDeckInfo.AppendLine("Main deck info: <br />");
                foreach (Card card in MainDeck.GetCards())
                {
                    FullDeckInfo.AppendFormat("{0}<br />", card.Identify());
                }

                ViewBag.FullDeckInfoBag = FullDeckInfo.ToString();
            }

            Deck PlayerDeck = (Deck)Session["PlayerDeck"];
            Deck HostileDeck = (Deck)Session["HostileDeck"];

            if (PlayerDeck == null)
                ViewBag.PlayerDeckInfoBag = "Player's deck is empty.";
            else
            {
                StringBuilder PlayerDeckInfo = new StringBuilder().Append("Player deck: <br />");
                foreach (Card card in PlayerDeck.GetCards())
                    PlayerDeckInfo.AppendFormat("{0}<br />", card.Identify());

                ViewBag.PlayerDeckInfoBag = PlayerDeckInfo.ToString();
            }

            if (HostileDeck == null)
                ViewBag.HostileDeckInfoBag = "Hostile's deck is empty.";
            else
            {
                StringBuilder HostileDeckInfo = new StringBuilder().Append("Hostile deck: <br />");
                foreach (Card card in HostileDeck.GetCards())
                    HostileDeckInfo.AppendFormat("{0}<br />", card.Identify());

                ViewBag.HostileDeckInfoBag = HostileDeckInfo.ToString();
            }

            Deck PlayerUsedCards = (Deck)Session["CurrentPlayerUsedCards"];
            Deck HostileUsedCards = (Deck)Session["CurrentHostileUsedCards"];

            if (PlayerUsedCards == null)
                ViewBag.PlayerUsedCardsInfoBag = "Players's used deck is empty.";
            else
            {
                StringBuilder PlayerUsedCardsInfo = new StringBuilder().Append("Player used cards: <br />");
                foreach (Card card in PlayerUsedCards.GetCards())
                    PlayerUsedCardsInfo.AppendFormat("{0}<br />", card.Identify());

                ViewBag.PlayerUsedCardsInfoBag = PlayerUsedCardsInfo.ToString();
            }

            if (HostileUsedCards == null)
                ViewBag.HostileUsedCardsInfoBag = "Hostile's used deck is empty.";
            else
            {
                StringBuilder HostileUsedCardsInfo = new StringBuilder().Append("Hostile used cards: <br />");
                foreach (Card card in HostileUsedCards.GetCards())
                    HostileUsedCardsInfo.AppendFormat("{0}<br />", card.Identify());

                ViewBag.HostileUsedCardsInfoBag = HostileUsedCardsInfo.ToString();
            }

            #endregion

            //Load decks info
            ViewBag.HostileDeck = HostileDeck != null ? HostileDeck.CardsCount : 0;
            ViewBag.HostileUsedDeck = HostileUsedCards != null ? HostileUsedCards.CardsCount : 0;
            ViewBag.PlayerDeck = PlayerDeck != null ? PlayerDeck.CardsCount : 0;
            ViewBag.PlayerUsedDeck = PlayerUsedCards != null ? PlayerUsedCards.CardsCount : 0;

            return View();
        }

        [HttpGet]
        public ActionResult LoadNewDeck()
        {
            Session["FullDeck"] = new Deck();
            return RedirectToAction("Play", "Game");
        }

        [HttpGet]
        public ActionResult ShuffleDeck()
        {
            if (Session["FullDeck"] != null)
            {
                Deck MainDeck = (Deck)Session["FullDeck"];
                MainDeck.Shuffle();
                Session["FullDeck"] = MainDeck;
            }

            return RedirectToAction("Play", "Game");
        }

        [HttpGet]
        public ActionResult SortDeck()
        {
            if (Session["FullDeck"] != null)
            {
                Deck MainDeck = (Deck)Session["FullDeck"];
                MainDeck.Sort();
                Session["FullDeck"] = MainDeck;
            }
            return RedirectToAction("Play", "Game");
        }

        [HttpGet]
        public ActionResult DivideDeck()
        {
            if (Session["FullDeck"] != null)
            {
                Deck MainDeck = (Deck)Session["FullDeck"];
                Deck PlayerDeck = new Deck(new List<Card>());
                Deck HostileDeck = new Deck(new List<Card>());
                Deck CurrentPlayerUsedCards = new Deck(new List<Card>());
                Deck CurrentHostileUsedCards = new Deck(new List<Card>());
                List<Card> CurrentHostileStack = new List<Card>();
                List<Card> CurrentPlayerStack = new List<Card>();

                DeckManager.SplitDeck(MainDeck, ref PlayerDeck, ref HostileDeck);

                Session["FullDeck"] = null;
                Session["PlayerDeck"] = PlayerDeck;
                Session["HostileDeck"] = HostileDeck;
                Session["CurrentPlayerUsedCards"] = CurrentPlayerUsedCards;
                Session["CurrentHostileUsedCards"] = CurrentHostileUsedCards;
                Session["CurrentHostileStack"] = null;
                Session["CurrentPlayerStack"] = null;
                Session["GameStatus"] = null;
                Session["LastBattleResult"] = null;
            }
            return RedirectToAction("Play", "Game");
        }

        [HttpGet]
        public ActionResult DrawCards()
        {
            // 1. Check last battle result
            LastGameResult LGR = Session["LastBattleResult"] != null ? (LastGameResult)Session["LastBattleResult"] : LastGameResult.Null;
            GameStatus StatusOfCurrentGame = Session["GameStatus"] != null ? (GameStatus)Session["GameStatus"] : GameStatus.InGame;

            // 2. Load game data
            Deck HostileDeck = Session["HostileDeck"] != null ? (Deck)Session["HostileDeck"] : new Deck(new List<Card>());
            Deck PlayerDeck = Session["PlayerDeck"] != null ? (Deck)Session["PlayerDeck"] : new Deck(new List<Card>());
            Deck CurrentPlayerUsedCards = Session["CurrentPlayerUsedCards"] != null ? (Deck)Session["CurrentPlayerUsedCards"] : new Deck(new List<Card>());
            Deck CurrentHostileUsedCards = Session["CurrentHostileUsedCards"] != null ? (Deck)Session["CurrentHostileUsedCards"] : new Deck(new List<Card>());
            List<Card> CurrentHostileStack = Session["CurrentHostileStack"] != null ? (List<Card>)Session["CurrentHostileStack"] : new List<Card>();
            List<Card> CurrentPlayerStack = Session["CurrentPlayerStack"] != null ? (List<Card>)Session["CurrentPlayerStack"] : new List<Card>(); ;

            // 3. Put cards in right stack
            if (LGR == LastGameResult.HostileWin)
            {
                CurrentHostileUsedCards = DeckManager.ConnectDecks(CurrentHostileUsedCards, CurrentHostileStack, CurrentPlayerStack);
                Session["CurrentHostileUsedCards"] = CurrentHostileUsedCards;

                CurrentHostileStack.Clear();
                CurrentPlayerStack.Clear();
            }
            else if (LGR == LastGameResult.PlayerWin)
            {
                CurrentPlayerUsedCards = DeckManager.ConnectDecks(CurrentPlayerUsedCards, CurrentHostileStack, CurrentPlayerStack);
                Session["CurrentPlayerUsedCards"] = CurrentPlayerUsedCards;

                CurrentHostileStack.Clear();
                CurrentPlayerStack.Clear();
            }

            // 4. If deck is empty, shuffle and get back cards
            if (HostileDeck.CardsCount == 0 && CurrentHostileUsedCards.CardsCount > 0)
            {
                //Remove worst card
                int TheWorstCardValue = CurrentHostileUsedCards.GetCards().Min(x => x.GetIntValue());
                Card TheWorstCard = CurrentHostileUsedCards.GetCards().Where(x => x.GetIntValue() == TheWorstCardValue).First();
                CurrentHostileUsedCards.RemoveCard(TheWorstCard);

                CurrentHostileUsedCards.Shuffle();
                HostileDeck.LoadCards(CurrentHostileUsedCards.GetCards());
                CurrentHostileUsedCards.ClearDeck();
                Session["CurrentHostileUsedCards"] = CurrentHostileUsedCards;
            }

            if (PlayerDeck.CardsCount == 0 && CurrentPlayerUsedCards.CardsCount > 0)
            {
                //Remove worst card
                int TheWorstCardValue = CurrentPlayerUsedCards.GetCards().Min(x => x.GetIntValue());
                Card TheWorstCard = CurrentPlayerUsedCards.GetCards().Where(x => x.GetIntValue() == TheWorstCardValue).First();
                CurrentPlayerUsedCards.RemoveCard(TheWorstCard);

                CurrentPlayerUsedCards.Shuffle();
                PlayerDeck.LoadCards(CurrentPlayerUsedCards.GetCards());
                CurrentPlayerUsedCards.ClearDeck();
                Session["CurrentPlayerUsedCards"] = CurrentPlayerUsedCards;
            }

            // 5. Check if somebody wins
            if (HostileDeck.CardsCount == 0 && CurrentHostileUsedCards.CardsCount == 0)
                StatusOfCurrentGame = GameStatus.PlayerWon;
            else if (PlayerDeck.CardsCount == 0 && CurrentPlayerUsedCards.CardsCount == 0)
                StatusOfCurrentGame = GameStatus.HostileWon;
            else          
            {
                // 6. Draw cards
                Card DrawnHostileCard = HostileDeck.Draw;
                Card DrawnPlayerCard = PlayerDeck.Draw;

                HostileDeck.RemoveCard(DrawnHostileCard);
                PlayerDeck.RemoveCard(DrawnPlayerCard);

                CurrentHostileStack.Add(DrawnHostileCard);
                CurrentPlayerStack.Add(DrawnPlayerCard);

                Session["HostileDeck"] = HostileDeck;
                Session["PlayerDeck"] = PlayerDeck;
                Session["CurrentHostileStack"] = CurrentHostileStack;
                Session["CurrentPlayerStack"] = CurrentPlayerStack;

                // 7. Battle cards
                Session["LastBattleResult"] = DeckManager.FightTwoCards(DrawnHostileCard, DrawnPlayerCard);
            }

            Session["GameStatus"] = StatusOfCurrentGame;

            return RedirectToAction("Play", "Game");
        }

        [HttpGet]
        public ActionResult EndGame()
        {
            //Clear game data
            Session.Clear();
            Session.Abandon();
            Session["GameStatus"] = GameStatus.InGame;

            return RedirectToAction("Play", "Game");
        }

    }
}